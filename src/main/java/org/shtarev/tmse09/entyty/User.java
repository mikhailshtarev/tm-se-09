package org.shtarev.tmse09.entyty;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse09.сommands.UserRole;

import java.util.UUID;

@Getter
@Setter
public class User {

    @Nullable
    private String name;

    @Nullable
    private String password;

    @Nullable
    private UserRole[] userRole;

    @NotNull
    private final String userId = UUID.randomUUID().toString();


}
