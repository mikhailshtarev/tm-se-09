package org.shtarev.tmse09.entyty;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse09.сommands.TaskProjectStatus;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
public class Project {

    @NotNull
    public final String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private LocalDate dataStart;

    @Nullable
    private LocalDate dataFinish;

    @Nullable
    private String userId;

    @Nullable
    private TaskProjectStatus[] taskProjectStatus;

    @Nullable
    private LocalDate dataCreate;
}
