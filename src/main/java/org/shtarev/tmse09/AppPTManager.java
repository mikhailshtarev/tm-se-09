package org.shtarev.tmse09;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.shtarev.tmse09.сommands.AbstractCommand;

import java.util.Collections;
import java.util.Set;

public class AppPTManager {


    public static void main(String[] args) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("org.shtarev.tmse08.сommands")).setScanners(new SubTypesScanner()));
        Set<Class<?>> classes;
        classes = Collections.unmodifiableSet(reflections.getSubTypesOf(AbstractCommand.class));
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.createTwoUser();
        bootstrap.init(classes);
        bootstrap.start();
    }
}
