package org.shtarev.tmse09.сommands;

import org.shtarev.tmse09.entyty.Project;
import org.shtarev.tmse09.entyty.Task;
import org.shtarev.tmse09.entyty.User;
import org.shtarev.tmse09.service.ProjectService;
import org.shtarev.tmse09.service.TaskService;
import org.shtarev.tmse09.service.UserService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    ProjectService<Project> getProjectService();

    TaskService<Task> getTaskService();

    List<AbstractCommand> getCommands();

    UserService<User> getUserService();

    User getBootUser();

    void setBootUser(User thisUser);

    Scanner getTerminalService();
}
